# ==============================================================================
# LIBRARIES
# ==============================================================================
# Numpy
import numpy as np
# Itertools
import itertools
# Copy
import copy
# Multiprocessing
from multiprocessing import Pool
# OS
import os

# test score 0.5327: f=100, c=1e-2, learning_rate=1e-2, train_ratio=0.8
# test score 0.5053: f=100, c=2.5e-2, learning_rate=1e-2, train_ratio=0.8


class RecommenderSystem():
    def __init__(
            self,
            train_ratio=0.8,
            f=[200],  #[50, 100, 200],
            c=[0.02],  #[0.05, 0.02, 0.01],
            learning_rate=[0.01],  #[0.05, 0.02, 0.01],
            mccv_runs=1,
            weight_option="unique",
            max_iter={
                "train": 50,
                "model": 200
            },
            tol=1e-6,
            threads=1):
        # init parameters
        self.train_ratio = train_ratio
        self.grid_ranges = {"c": c, "f": f, "learning_rate": learning_rate}
        self.mccv_runs = mccv_runs
        self.weight_option = weight_option
        self.max_iter = max_iter
        self.tol = tol
        self.threads = threads

    def import_data(self, src):
        data_raw = np.genfromtxt(src, delimiter=",")
        self.n_obs = data_raw.shape[0]
        self.data_raw = data_raw
        self._preproc_raw_data()

    def _export_grid_search(self, grid_search):
        grid_search_results = np.zeros((len(grid_search), 5))
        for row, point in enumerate(grid_search):
            grid_search_results[row, 0] = point['f']
            grid_search_results[row, 1] = point['c']['x']
            grid_search_results[row, 2] = point['learning_rate']
            grid_search_results[row, 3] = point['train_rmse']
            grid_search_results[row, 4] = point['test_rmse']
        np.savetxt(
            "train_test_gridsearch.csv",
            grid_search_results,
            header="f,c,learning_rate,train_rmse,test_rmse",
            delimiter=',',
            comments='')

    def _unique_arr_to_dict(self, arr):
        arr_dict = dict(np.c_[arr, np.full(len(arr), -1)])
        id = 0
        for key, val in arr_dict.items():
            if -1 == val:
                arr_dict[key] = id
                id += 1
        return arr_dict

    def _weighting_unique(self, r):
        return 1.0

    def _weighting_log(self, r):
        return 1.0 + np.log(1 + r)

    def _weighting(self, r):
        weighting_dict = {
            "unique": self._weighting_unique,
            "log": self._weighting_log
        }
        return weighting_dict[self.weight_option](r)

    def _preproc_raw_data(self):
        # extract user/item raw
        user_raw = np.unique(self.data_raw[:, 0])
        item_raw = np.unique(self.data_raw[:, 1])
        # n_u, n_i
        self.n = len(self.data_raw)
        self.n_u = len(user_raw)
        self.n_i = len(item_raw)
        print("Users:", self.n_u, " Items:", self.n_i)
        # define dictionaries: user, item -> int id
        self.user_dict = self._unique_arr_to_dict(user_raw)
        self.item_dict = self._unique_arr_to_dict(item_raw)
        # transform data_raw to data
        data = self.data_raw
        weight = np.ones(self.n)
        for idx, d in enumerate(data):
            u = d[0]
            i = d[1]
            data[idx, 0] = self.user_dict[u]
            data[idx, 1] = self.item_dict[i]
            r = d[2]
            weight[idx] = self._weighting(r)

        self.data_u = data[:, 0].astype(np.int)
        self.data_i = data[:, 1].astype(np.int)
        self.data_w = weight
        # get mean rating of total observed data
        self.data_r = data[:, 2]
        self.r_mean = np.mean(self.data_r)
        self.data_r -= self.r_mean

    def _predict(self, bias_x, bias_y, x, y):
        return bias_x + bias_y + np.dot(x, y)

    def _rmse(self, idx, bias_x, bias_y, x, y):
        # root mean squared error
        rmse = 0.0
        for u, i, r in zip(self.data_u[idx], self.data_i[idx],
                           self.data_r[idx]):
            rmse += np.power(
                r - self._predict(bias_x[u], bias_y[i], x[u], y[i]), 2)
        rmse = np.sqrt(rmse / len(idx))
        return rmse

    def _get_grid(self):
        # get grid ranges of f and alpha
        # f, alpha ranges
        learning_rate_space = self.grid_ranges['learning_rate']
        f_space = self.grid_ranges['f']
        c_space = self.grid_ranges['c']
        points = list()
        for learning_rate in learning_rate_space:
            for f in f_space:
                for c in c_space:
                    point = {
                        "learning_rate": learning_rate,
                        "f": f,
                        "c": {
                            "bias_x": c,
                            "bias_y": c,
                            "x": c,
                            "y": c
                        },
                        "test_rmse": 0.0,
                        "train_rmse": 0.0
                    }
                    points.append(point)
        return points

    def _sgd(self, train_idx, c, f, learning_rate, option="train"):
        # get regularization coefficients
        c_bias_x = c['bias_x']
        c_bias_y = c['bias_y']
        c_x = c['x']
        c_y = c['y']
        # init bias to zero
        bias_x = np.zeros(self.n_u)
        bias_y = np.zeros(self.n_i)
        # init x,y to normal dist vectors
        x = np.random.normal(
            loc=0.0, scale=np.sqrt(1.0 / f), size=(self.n_u, f))
        y = np.random.normal(
            loc=0.0, scale=np.sqrt(1.0 / f), size=(self.n_i, f))
        # run stochastic gradient descent
        tol_iter = np.finfo(np.float64).max
        score = self._rmse(train_idx, bias_x, bias_y, x, y)
        iter = 0
        while tol_iter >= self.tol and iter < self.max_iter[option]:
            iter += 1
            np.random.shuffle(train_idx)
            for u, i, r, w in zip(
                    self.data_u[train_idx], self.data_i[train_idx],
                    self.data_r[train_idx], self.data_w[train_idx]):
                # calc bias
                bias = r - (bias_x[u] + bias_y[i] + np.dot(x[u], y[i]))
                # update parameters
                wbias = w * bias
                bias_x[u] += learning_rate * (wbias - c_bias_x * bias_x[u])
                bias_y[i] += learning_rate * (wbias - c_bias_y * bias_y[i])
                x[u] += learning_rate * (wbias * y[i] - c_x * x[u])
                y[i] += learning_rate * (wbias * x[u] - c_y * y[i])

            if iter % 10 == 1 or iter == self.max_iter[option]:
                score_iter = self._rmse(train_idx, bias_x, bias_y, x, y)
                tol_iter = np.abs((score_iter - score) / score_iter)
                score = score_iter
                print("Iteration:", iter, ", Train Score:", score)
            if iter == self.max_iter[option] and option == "train":
                test_idx = np.delete(np.arange(self.n), train_idx)
                test_score = self._rmse(test_idx, bias_x, bias_y, x, y)

        if option == "train":
            train_score = score
            test_idx = np.delete(np.arange(self.n), train_idx)
            test_score = self._rmse(test_idx, bias_x, bias_y, x, y)
            return train_score, test_score
        if option == "model":
            return bias_x, bias_y, x, y, score

    def _get_mccv_train_idx(self, run):
        train_size = int(self.train_ratio * self.n)
        idx = np.arange(self.n)
        np.random.seed(run)
        np.random.shuffle(idx)
        return idx[:train_size]

    def _grid_search(self, run):
        print("Monte Carlo Cross-Validation Run:", run)
        # get grid points with their empty score keys
        grid_points = self._get_grid()
        # get local train data set
        train_idx = self._get_mccv_train_idx(run)
        # grid search
        for i, p in enumerate(grid_points):
            c = p['c']
            f = p['f']
            learning_rate = p['learning_rate']
            train_rmse, test_rmse = self._sgd(
                train_idx, c, f, learning_rate, option="train")
            grid_points[i]['train_rmse'] = train_rmse
            grid_points[i]['test_rmse'] = test_rmse
        return grid_points

    def _mccv(self):
        # run monte carlo cross validation
        pool = Pool(self.threads)
        mccv_grid_search = pool.map(self._grid_search, range(self.mccv_runs))
        mccv_grid_search_mean = self._get_grid()
        for grid_search in mccv_grid_search:
            for i, point in enumerate(grid_search):
                mccv_grid_search_mean[i]['train_rmse'] += point['train_rmse']
                mccv_grid_search_mean[i]['test_rmse'] += point['test_rmse']
        # normalize by self.mccv_runs
        for i in range(len(mccv_grid_search_mean)):
            mccv_grid_search_mean[i]['train_rmse'] /= self.mccv_runs
            mccv_grid_search_mean[i]['test_rmse'] /= self.mccv_runs
        # find f, c, learning rate with best (minimal) mean test-score
        best_point = mccv_grid_search_mean[np.argmin(
            [el['test_rmse'] for el in mccv_grid_search_mean])]
        self.best_grid_point = best_point
        # export grid_search results
        self._export_grid_search(mccv_grid_search_mean)

    def fit(self, option='grid_search'):
        # monte carlo cross-validation grid search for f, alpha
        if option == 'grid_search':
            self._mccv()
        else:
            grid_points = self._get_grid()
            self.best_grid_point = grid_points[0]

        c = self.best_grid_point['c']
        f = self.best_grid_point['f']
        learning_rate = self.best_grid_point['learning_rate']
        print("Get Model for f:", f, " ,c:", c, " ,learning_rate:",
              learning_rate)
        # get x_tilde, y_tilde for best f, alpha trained by total data set
        total_idx = np.arange(self.n)
        self.bias_x, self.bias_y, self.x, self.y, self.score = self._sgd(
            total_idx, c, f, learning_rate, option="model")

        print("Score:", self.score)

    def predict(self, data):
        r = np.zeros(len(data))
        for idx, d in enumerate(data):
            u = int(d[0])
            i = int(d[1])
            if u in self.user_dict and i in self.item_dict:
                u = self.user_dict[u]
                i = self.item_dict[i]
                r_idx = self.r_mean + self._predict(
                    self.bias_x[u], self.bias_y[i], self.x[u], self.y[i])
            elif u in self.user_dict:
                u = self.user_dict[u]
                r_idx = self.r_mean + self.bias_x[u]
                print("Item:", i, " does not exist in train data")
            elif i in self.item_dict:
                r_idx = self.r_mean + self.bias_y[i]
                print("User:", u, " does not exist in train data")
            else:
                r_idx = self.r_mean
                print("Item:", i, " and user:", u,
                      " don't exist in train data")
            r[idx] = np.max([0, np.min([r_idx, 4])])
        return np.c_[data, r]

    def predict_data_csv(self, src):
        data = np.genfromtxt(src, delimiter=",")
        data_predict = self.predict(data)
        np.savetxt(
            src.split('.csv')[0] + "_prediction_new.csv",
            data_predict,
            header="user, item, rating",
            delimiter=',',
            comments='')


def main():
    rs = RecommenderSystem()
    rs.import_data("train.csv")
    rs.fit(option="model")
    rs.predict_data_csv("qualifying.csv")


if __name__ == '__main__':
    main()
