# Recommender System

A python program building a recommender system by learning a model via biased matrix factorization performed by stochastic gradient descent and selected by a monte carlo cross-validation grid search.

## Getting Started

### Prerequisites

Get the prerequisites from python using pip
```
pip install -r requirements.txt
```

## Literature

See the theory behind the recommender system in literature.pdf.

## Built With

* [NumPy](https://www.numpy.org/) - a fundamental package for scientific computing with Python

### License

This project is licensed under the MIT license.
