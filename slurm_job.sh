#!/bin/bash
#SBATCH --job-name=lab-recommender-system
#SBATCH -p standard # partition (queue)
#SBATCH -N 1 # number of nodes
#SBATCH --exclusive
#SBATCH -n 24 # number of cores
#SBATCH --mem 48 # memory pool for all cores
#SBATCH -t 0-20:00 # time (D-HH:MM)
python3 recommender-system.py
